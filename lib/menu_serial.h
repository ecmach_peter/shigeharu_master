
const String s_menu_logo PROGMEM = "** SHIGEHARU 建機智能管理 **\n";
const String s_menu_exit PROGMEM = "9) 離開\n\n";
const String s_menu_pwd PROGMEM = s_menu_logo + "   請輸入密碼\n\n";

const String s_menu_top PROGMEM = s_menu_logo + "1) 輸入硬體類別\n2) 輸入硬體序號\n3) 輸入硬體地址\n4) 輸入軟體版本\n8) 顯示資訊\n" + s_menu_exit;
const String s_menu_t1 PROGMEM = s_menu_logo + "** 輸入硬體類別\n" + s_menu_exit;
const String s_menu_t2 PROGMEM = s_menu_logo + "** 輸入硬體序號\n" + s_menu_exit;
const String s_menu_t3 PROGMEM = s_menu_logo + "** 輸入硬體地址\n" + s_menu_exit;
const String s_menu_t4 PROGMEM = s_menu_logo + "** 輸入軟體版本\n" + s_menu_exit;
const String s_menu_t5 PROGMEM = s_menu_logo + "** 產品資訊\n";
const String s_menu_t6 PROGMEM = s_menu_logo + "** FACTORY DEFAULT\n";


const char* s_menu_home_display0[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char* s_menu_home_display1[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char* s_menu_home_display2[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char* s_menu_home_display3[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char s_menu_home_result[] = {0, 1, 2, 3};
const char** s_menu_home_list[] = {s_menu_home_display0, s_menu_home_display1, s_menu_home_display2, s_menu_home_display3};