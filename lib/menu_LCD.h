const String l_menu_logo PROGMEM = " SHIGEHARU System ";
const String l_menu_exit PROGMEM = "9) 離開\n\n";
const String l_menu_pwd PROGMEM = l_menu_logo + "   請輸入密碼\n\n";

const String l_menu_top PROGMEM = l_menu_logo + "1) 輸入硬體類別\n2) 輸入硬體序號\n3) 輸入硬體地址\n4) 輸入軟體版本\n8) 顯示資訊\n" + l_menu_exit;
const String l_menu_t1 PROGMEM = l_menu_logo + "** 輸入硬體類別\n" + l_menu_exit;
const String l_menu_t2 PROGMEM = l_menu_logo + "** 輸入硬體序號\n" + l_menu_exit;
const String l_menu_t3 PROGMEM = l_menu_logo + "** 輸入硬體地址\n" + l_menu_exit;
const String l_menu_t4 PROGMEM = l_menu_logo + "** 輸入軟體版本\n" + l_menu_exit;
const String l_menu_t5 PROGMEM = l_menu_logo + "** 產品資訊\n";
const String l_menu_t6 PROGMEM = l_menu_logo + "** FACTORY DEFAULT\n";


const char* l_menu_home_display0[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char* l_menu_home_display1[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char* l_menu_home_display2[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char* l_menu_home_display3[] = {"Display 1", "Display 1", "Display 1", "Display 1"};
const char l_menu_home_result[] = {0, 1, 2, 3};
const char** l_menu_home_list[] = {l_menu_home_display0, l_menu_home_display1, l_menu_home_display2, l_menu_home_display3};