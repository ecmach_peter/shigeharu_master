
void EE_write_str(char addr, String data)
{
  delay(100);
  int _size = data.length();
  int i;
  for (i = 0; i < _size; i++)
  {
    EEPROM.update(addr + i, data[i]);
  }
  if (_size > 0) {
    _size--;
  }
  EEPROM.update(addr + _size, '\0'); //Add termination null character for String Data
  delay(100);
}


String EE_read_str(char addr)
{
  delay(100);
  int i;
  char data[0x10]; //Max 100 Bytes
  int len = 0;
  unsigned char k;
  k = EEPROM.read(addr);
  while (k != '\0' && len < 0x10) //Read until null character
  {
    k = EEPROM.read(addr + len);
    data[len] = k;
    len++;
  }
  delay(100);
  data[len] = '\0';
  return String(data);
}
