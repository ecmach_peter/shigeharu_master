#include <Wire.h>
#include <EEPROM.h>

#include "lib/config.h"
#include "lib/hardware.h"
#include "lib/EEPROM_misc.c"
#include "lib/menu_LCD.h"
#include "lib/menu_serial.h"
#include  "lib/LiquidCrystal_I2C.h"
#include  "lib/LiquidCrystal_I2C.c"

LiquidCrystal_I2C lcd(0x27, 20, 4);
int timer1_counter;
int timer2_counter;

void timer1_init() {
  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  // Set timer1_counter to the correct value for our interrupt interval
  //timer1_counter = 64911;   // preload timer 65536-16MHz/256/100Hz
  //timer1_counter = 64286;   // preload timer 65536-16MHz/256/50Hz
  timer1_counter = 65411;   // preload timer 65536-16MHz/256/2Hz

  TCNT1 = timer1_counter;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}
void timer2_init() {
  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR2A = 0;
  TCCR2B = 0;

  // Set timer1_counter to the correct value for our interrupt interval
  //timer1_counter = 64911;   // preload timer 65536-16MHz/256/100Hz
  //timer1_counter = 64286;   // preload timer 65536-16MHz/256/50Hz
  //timer2_counter = 64286;   // preload timer 65536-16MHz/256/2Hz
  timer2_counter = 65286; // preload timer 65536-16MHz/256/250Hz

  TCNT2 = timer2_counter;   // preload timer
  TCCR2B |= (1 << CS12);    // 256 prescaler
  TIMSK2 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
}

byte G_output_lv = 1;
byte G_output_lv_prev = 0;
void output() {
  if (G_output_lv != G_output_lv) {
    digitalWrite(CONF_OUTPUT_R, LOW);
    digitalWrite(CONF_OUTPUT_Y, LOW);
    digitalWrite(CONF_OUTPUT_G, LOW);
    digitalWrite(CONF_OUTPUT_BUZZER, LOW);
    G_output_lv_prev = G_output_lv;
  }
  if (G_output_lv == 1) {
    digitalWrite(CONF_OUTPUT_G, digitalRead(CONF_OUTPUT_G) ^ 1);
  } else if (G_output_lv == 2) {
    digitalWrite(CONF_OUTPUT_Y, digitalRead(CONF_OUTPUT_Y) ^ 1);
    digitalWrite(CONF_OUTPUT_BUZZER, digitalRead(CONF_OUTPUT_BUZZER) ^ 1);
  } else if (G_output_lv == 3) {
    digitalWrite(CONF_OUTPUT_BUZZER, HIGH);
    digitalWrite(CONF_OUTPUT_R, digitalRead(CONF_OUTPUT_R) ^ 1);
  } else {
    digitalWrite(CONF_OUTPUT_R, LOW);
    digitalWrite(CONF_OUTPUT_Y, LOW);
    digitalWrite(CONF_OUTPUT_G, LOW);
  }
}

ISR(TIMER1_OVF_vect)        // interrupt service routine
{
  TCNT1 = timer1_counter;   // preload timer
  output();
}

volatile byte PB_state = 0;
volatile byte timer2_cnt = 0;
volatile byte input_change = 0;
volatile byte rotary_state = 0;
byte rotary_state_prev = 0;
#define C_DEBOUND 0x03
ISR(TIMER2_OVF_vect)        // interrupt service routine
{
  TCNT2 = timer2_counter;   // preload timer
  timer2_cnt = 1;
}

volatile unsigned int encoder0Pos = 0;
void rotary_encoder() {
  byte x = PIND;
  x = x >> 2;
  x &= 0x03;
  rotary_state = rotary_state << 2;
  rotary_state |= x;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(CONF_SERVICE_PIN, INPUT);
  pinMode(CONF_ENCODER0, INPUT);
  pinMode(CONF_ENCODER1, INPUT);
  pinMode(CONF_MENU_PIN, INPUT);
  pinMode(CONF_OUTPUT_R, OUTPUT);
  pinMode(CONF_OUTPUT_Y, OUTPUT);
  pinMode(CONF_OUTPUT_G, OUTPUT);
  pinMode(CONF_OUTPUT_BUZZER, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  lcd.init();
  timer1_init();
  timer2_init();
  attachInterrupt(0, rotary_encoder, CHANGE);
  attachInterrupt(1, rotary_encoder, CHANGE);
}

void service_display_mode() {
  String x = "";
  x = EE_read_str(EE_START_ADDR_HARDWARE_TYPE);
  Serial.println("硬體類別: " + x);
  x = EE_read_str(EE_START_ADDR_HARDWARE_SERIAL);
  Serial.println("硬體序號: " + x);
  x = EE_read_str(EE_START_ADDR_HARDWARE_ADDR);
  Serial.println("硬體地址: " + x);
  x = EE_read_str(EE_START_ADDR_SOFTWARE_VER);
  Serial.println("軟體版本: " + x);
}

void service_setting_mode() {
  byte result = 0xFF;
  byte type = 0;
  byte EE_start_addr = 0;
  String answer = "";
  while (1) {
    type = 0;
    Serial.println(s_menu_top);
    while (!type) {
      while (!Serial.available()) {}
      answer = Serial.readString();

      if (answer.equals("1\n")) {
        Serial.println(s_menu_t1);
        type = 1;
      } else if (answer.equals("2\n")) {
        Serial.println(s_menu_t2);
        type = 2;
      } else if (answer.equals("3\n")) {
        Serial.println(s_menu_t3);
        type = 3;
      } else if (answer.equals("4\n")) {
        Serial.println(s_menu_t4);
        type = 4;
      } else if (answer.equals("M\n")) {
        Serial.println("init EEPROM");
        int i;
        for (i = 0; i < 0x80; i++)
        {
          EEPROM.write(0x80 + i, 0x00);
        }
      } else if (answer.equals("8\n")) {
        Serial.println(s_menu_t5);
        service_display_mode();
      } else if (answer.equals("9\n")) {
        Serial.println("exit");
        delay(100);
        exit(0);
      }
    }
    while (!Serial.available()) {}
    answer = Serial.readString();
    Serial.println(answer);
    if (type == 1) {
      EE_start_addr  = EE_START_ADDR_HARDWARE_TYPE;
    } else if (type == 2) {
      EE_start_addr  = EE_START_ADDR_HARDWARE_SERIAL;
    } else if (type == 3) {
      EE_start_addr  = EE_START_ADDR_HARDWARE_ADDR;
    } else if (type == 4) {
      EE_start_addr  = EE_START_ADDR_SOFTWARE_VER;
    }
    EE_write_str(EE_start_addr, answer);
    /*
       EE write String
    */
  }
}
void service_mode() {
  byte result = 1;
  Serial.println(s_menu_pwd);
  while (result) {
    while (!Serial.available()) {}
    String answer = Serial.readString();
    if (answer.equals("21398586\n")) {
      service_display_mode();
    }
    else if (answer.equals("z864sf3!\n")) {
      service_setting_mode();
      result = 0;
    }
    else {
      result++;
      Serial.println("Error");
      if (result > 3) {
        Serial.println("Contact System Admin");
        while (1);
      }
    }
  }
}
void chk_rotary_encoder() {
  if (rotary_state != rotary_state_prev) {
    if ((rotary_state == 0x2D) || (rotary_state == 0xB4) || (rotary_state == 0xD2) || (rotary_state == 0xB4)) {
      input_change |= 0x81;
    } else if ((rotary_state == 0xE1) || (rotary_state == 0x87) || (rotary_state == 0x1E) || (rotary_state == 0x78)) {
      input_change |= 0x82;
    }
    rotary_state_prev = rotary_state;
  }

}

void update_lcd_cursor(byte x) {
  Serial.println(x);
  for (int i = 0; i <= 3; i++) {
    lcd.setCursor(0, i);
    if (i == x ) {
      lcd.print("*");
    } else {
      lcd.print(" ");
    }
  }
}

void update_lcd_menu(byte x) {
  lcd.clear();
  switch (x) {
    case 0:
      lcd.setCursor(0, 0);
      lcd.print(l_menu_logo);
      lcd.setCursor(0, 1);break;
    
    default:
      break;
  }

}
byte LCD_cursor = 0;
byte LCD_menu = 0;

void loop() {
  // put your main code here, to run repeatedly:
  byte val = 0;
  byte x, y, z = 0;
  lcd.backlight();
  update_lcd_menu(0);
  delay(500);
  input_change = 0;
  // enable all interrupts
  interrupts();

  while (1) {
    chk_rotary_encoder();
    if (timer2_cnt == 1) {
      timer2_cnt = 0;
    }
    if (input_change) {
      /*
      if (input_change & 0x02) {
        Serial.println(LCD_cursor);
        LCD_cursor--;
        val = LCD_cursor & 0x07;
        val = val >> 1;
        update_lcd_cursor(val);
      } else if (input_change & 0x01) {
        LCD_cursor++;
        val = LCD_cursor & 0x07;
        val = val >> 1;
        update_lcd_cursor(val);
      } else if (input_change & 0x04) {
        val = LCD_cursor & 0x07;
        val = val >> 1;
        val ++;
        update_lcd_menu(val);
      } else if (input_change & 0x08) {
        val = LCD_cursor & 0x07;
        val = val >> 1;
        val ++;
        update_lcd_menu(val);
      }
      */
      input_change = 0;
    }
  };
}
